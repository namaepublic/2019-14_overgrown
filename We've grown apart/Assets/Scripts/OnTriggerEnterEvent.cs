﻿using UnityEngine;
using UnityEngine.Events;

public class OnTriggerEnterEvent : MonoBehaviour
{
    [SerializeField] private GameObject _target;
    
    [SerializeField] private UnityEvent _event;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetInstanceID() == _target.GetInstanceID())
            _event.Invoke();
    }
}

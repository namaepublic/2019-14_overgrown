﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private int _sceneIndex;

    public void LoadScene() => LoadScene(_sceneIndex);
    
    public void LoadScene(int index) => SceneManager.LoadScene(index);
}

﻿using System;
using System.Linq;
using NaughtierAttributes;
using UnityEngine;

[Flags]
public enum Intent : byte
{
    Left = 0b1,
    Right = 0b10,
    Jump = 0b100,
}

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    private const float MIN_HEIGHT = -4.3f;
    private const float MAX_HEIGHT = 3.59f;

    [Header("Components")] [SerializeField]
    private Transform _graphicsTransform;

    [SerializeField, Hide, GetComponent] private Rigidbody2D _rigidbody2D;

    [Header("Keys")] [SerializeField] private KeyCode[] _leftKeys;
    [SerializeField] private KeyCode[] _rightKeys;
    [SerializeField] private KeyCode[] _jumpKeys;

    [Header("Movement stats")] [SerializeField]
    private float _speed;

    [SerializeField] private AnimationCurve _scaleByAltitude;

    [Header("Jump stats")] [SerializeField]
    private float _jumpForce;

    [SerializeField] private float _headJumpForce;
    [SerializeField] private float _groundCheckRadius;
    [SerializeField] private Transform _groundCheckTransform;
    [SerializeField] private float _headGroundedRadius;
    [SerializeField] private Transform _headGroundedTransform;
    [SerializeField] private LayerMask _groundLayerMask;

    private Intent m_intent;
    private bool m_isGrounded;
    private bool m_headGrounded;

    private float JumpForce => _jumpForce * transform.localScale.y;
    private float HeadJumpForce => _headJumpForce * transform.localScale.y;

    private float GroundCheckRadius => _groundCheckRadius * transform.localScale.y;
    private float HeadGroundedRadius => _headGroundedRadius * transform.localScale.y;

    private void Update()
    {
        if (CheckKeysDown(_leftKeys))
        {
            RemoveIntent(Intent.Right);
            AddIntent(Intent.Left);
        }

        if (CheckKeysDown(_rightKeys))
        {
            RemoveIntent(Intent.Left);
            AddIntent(Intent.Right);
        }

        if (CheckKeysUp(_leftKeys) && !CheckKeys(_leftKeys))
        {
            RemoveIntent(Intent.Left);
            CheckAndAddIntent(_rightKeys, Intent.Right);
        }

        if (CheckKeysUp(_rightKeys) && !CheckKeys(_rightKeys))
        {
            RemoveIntent(Intent.Right);
            CheckAndAddIntent(_leftKeys, Intent.Left);
        }

        CheckDownAndAddIntent(_jumpKeys, Intent.Jump);
    }

    private void FixedUpdate()
    {
        m_isGrounded = CheckGround();
        m_headGrounded = HeadGrounded();

#if UNITY_EDITOR
        if (m_isGrounded) Debug.Log("Grounded");
        if (m_headGrounded) Debug.Log("Head Grounded");
#endif
        
        var deltaTime = Time.deltaTime;
        var deltaSpeed = deltaTime * _speed;

        if (HasIntent(Intent.Left))
        {
            FlipX(true);
            _rigidbody2D.position += deltaSpeed * Vector2.left;
        }

        if (HasIntent(Intent.Right))
        {
            FlipX(false);
            _rigidbody2D.position += deltaSpeed * Vector2.right;
        }

        if (HasIntent(Intent.Jump))
        {
            if (m_isGrounded) _rigidbody2D.velocity = JumpForce * Vector2.up;
            else if (m_headGrounded) _rigidbody2D.velocity = HeadJumpForce * Vector2.up;
            RemoveIntent(Intent.Jump);
        }

        Scale(_rigidbody2D.position.y);
    }

    public void FlipX() => FlipX(transform.localScale.x > 0);

    public void FlipX(bool faceLeft)
    {
        var scale = _graphicsTransform.localScale;
        scale.x = faceLeft ? -Mathf.Abs(scale.x) : Mathf.Abs(scale.x);
        _graphicsTransform.localScale = scale;
    }

    public void Scale(float altitude)
    {
        var normalizedAltitude = Mathf.InverseLerp(MIN_HEIGHT, MAX_HEIGHT, altitude);

        var scale = transform.localScale;

        var newScale = _scaleByAltitude.Evaluate(normalizedAltitude);
        scale.x = scale.x < 0 ? -newScale : newScale;
        scale.y = newScale;

        transform.localScale = scale;
    }

    private bool CheckGround() =>
        Physics2D.OverlapCircle(_groundCheckTransform.position, GroundCheckRadius, _groundLayerMask);

    private bool HeadGrounded() =>
        Physics2D.OverlapCircle(_headGroundedTransform.position, HeadGroundedRadius, _groundLayerMask);

    private static bool CheckKeys(KeyCode[] keys) => keys.Any(key => Input.GetKey(key));

    private static bool CheckKeysDown(KeyCode[] keys) => keys.Any(key => Input.GetKeyDown(key));

    private static bool CheckKeysUp(KeyCode[] keys) => keys.Any(key => Input.GetKeyUp(key));

    private bool HasIntent(Intent intent) => (m_intent & intent) != 0;

    private void AddIntent(Intent intent) => m_intent |= intent;

    private void RemoveIntent(Intent intent) => m_intent &= ~intent;

    private void IsolateIntent(Intent intent) => m_intent &= intent;

    private bool CheckAndAddIntent(KeyCode[] keys, Intent intent)
    {
        if (!CheckKeys(keys)) return false;
        AddIntent(intent);
        return true;
    }

    private bool CheckDownAndAddIntent(KeyCode[] keys, Intent intent)
    {
        if (!CheckKeysDown(keys)) return false;
        AddIntent(intent);
        return true;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Scale(_rigidbody2D.position.y);
        
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(_groundCheckTransform.position, GroundCheckRadius);
        Gizmos.DrawWireSphere(_headGroundedTransform.position, HeadGroundedRadius);
    }
#endif
}
﻿namespace NaughtierAttributes
{
	public enum Align : byte
	{
		Left,
		Center,
		Right
	}

	public enum ColorValue
	{
		Default,
		Red,
		Pink,
		Orange,
		Yellow,
		Green,
		Blue,
		Indigo,
		Violet,
		White
	}
}

using System;

namespace NaughtierAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ShowIfAttribute : BaseDrawConditionAttribute
    {
        public string[] ConditionNames { get; private set; }
        
        public ShowIfAttribute(params string[] conditionNames)
        {
            ConditionNames = conditionNames;
        }
    }
}

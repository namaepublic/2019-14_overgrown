﻿#if UNITY_EDITOR
// This class is auto generated

using System;
using System.Collections.Generic;

namespace NaughtyAttributes.Editor
{
    public static class __classname__
    {
        private static Dictionary<Type, BasePropertyValidator> validatorsByAttributeType;

        static __classname__()
        {
            validatorsByAttributeType = new Dictionary<Type, BasePropertyValidator>();
            __entries__
        }

        public static BasePropertyValidator GetValidatorForAttribute(Type attributeType)
        {
            BasePropertyValidator validator;
            if (validatorsByAttributeType.TryGetValue(attributeType, out validator))
            {
                return validator;
            }
            else
            {
                return null;
            }
        }
    }
}
#endif
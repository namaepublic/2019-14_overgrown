#if UNITY_EDITOR
using UnityEditor;

namespace NaughtierAttributes.Editor
{
    [PropertyDrawCondition(typeof(HideIfAttribute))]
    public class HideIfPropertyDrawCondition : BasePropertyDrawCondition
    {
        public override bool CanDrawProperty(SerializedProperty property)
        {
            HideIfAttribute hideIfAttribute = PropertyUtility.GetAttribute<HideIfAttribute>(property);
            var target = PropertyUtility.GetTargetObject(property);
            
            var canDraw = true;
            foreach (var conditionName in hideIfAttribute.ConditionNames)
            {
                if (SerializedPropertyUtility.GetValueFromTypeInfo(target, conditionName, out bool temp))
                    canDraw = canDraw && !temp;
                else
                {
                    string warning = hideIfAttribute.GetType().Name + " needs a valid boolean condition field or method name to work";
                    EditorDrawUtility.DrawHelpBox(warning, MessageType.Warning, logToConsole: true, context: target);

                    return true; 
                }
            }
            return canDraw;
        }
    }
}
#endif